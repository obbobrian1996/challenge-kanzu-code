<?php

function removeDuplicatesFromFile($filename) {
    $fileContents = file_get_contents($filename);
    $words = preg_split('/\s+/', $fileContents);
    $uniqueWords = array_unique($words);
    return $uniqueWords;
}

// $filename = 'test-file.txt';
$filename = __DIR__ . '/test-file.txt'; // Specify the correct path here

$uniqueWordsArray = removeDuplicatesFromFile($filename);

print_r($uniqueWordsArray);

?>
