<?php

function extractPunctuationMarksFromFile($filename) {
    $fileContents = file_get_contents($filename);
    preg_match_all('/[[:punct:]]+/', $fileContents, $matches);
    return $matches[0];
}

// $filename = 'test-file.txt';
$filename = __DIR__ . '/test-file.txt'; // Specify the correct path here

$punctuationMarksArray = extractPunctuationMarksFromFile($filename);

print_r($punctuationMarksArray);

?>
