<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Function to extract an email address without using regex
function extractEmailWithoutRegex($text) {
    $words = explode(" ", $text);
    foreach ($words as $word) {
        if (filter_var($word, FILTER_VALIDATE_EMAIL)) {
            return $word;
        }
    }
    return null;
}

// Function to extract a phone number without using regex
function extractPhoneNumberWithoutRegex($text) {
    $words = explode(" ", $text);
    foreach ($words as $word) {
        $word = preg_replace('/[^0-9]/', '', $word);
        if (strlen($word) === 12 && ctype_digit($word)) {
            return $word;
        }
    }
    return null;
}

// Function to extract a URL without using regex
function extractUrlWithoutRegex($text) {
    $words = explode(" ", $text);
    foreach ($words as $word) {
        if (strpos($word, "http://") === 0 || strpos($word, "https://") === 0) {
            return $word;
        }
    }
    return null;
}



// Example Usage

$email = extractEmailWithoutRegex($paragraph);
$phoneNumber = extractPhoneNumberWithoutRegex($paragraph);
$url = extractUrlWithoutRegex($paragraph);

echo "Email: $email\n";
echo "Phone Number: $phoneNumber\n";
echo "URL: $url\n";

?>
