<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Function to extract an email address using regex
function extractEmailUsingRegex($text) {
    preg_match("/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/", $text, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}

// Function to extract a phone number using regex
function extractPhoneNumberUsingRegex($text) {
    preg_match("/\b\d{12}\b/", $text, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}

// Function to extract a URL using regex
function extractUrlUsingRegex($text) {
    preg_match("/https?:\/\/[^\s]+/", $text, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}



// Example Usage

$email = extractEmailUsingRegex($paragraph);
$phoneNumber = extractPhoneNumberUsingRegex($paragraph);
$url = extractUrlUsingRegex($paragraph);

echo "Email: $email\n";
echo "Phone Number: $phoneNumber\n";
echo "URL: $url\n";

?>
