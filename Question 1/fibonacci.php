<?php

// Function to generate the Fibonacci sequence up to a certain number of terms
function generateFibonacci($n)
{
    $fibArray = [0, 1];

    for ($i = 2; $i < $n; $i++) {
        $fibArray[$i] = $fibArray[$i - 1] + $fibArray[$i - 2];
    }

    return $fibArray;
}

// Create an array of numbers from 0 to 100
$numbers = range(0, 100);

// Sort the numbers in ascending order
sort($numbers);

// Generate Fibonacci numbers starting from the seventh number
$fibonacciNumbers = generateFibonacci(20); // Generating more terms to ensure we have at least 7

// Filter out Fibonacci numbers less than 100
$fibonacciNumbers = array_filter($fibonacciNumbers, function ($num) {
    return $num <= 100;
});

// Sort the Fibonacci numbers in descending order
rsort($fibonacciNumbers);

// Extract the first seven Fibonacci numbers
$finalFibonacciNumbers = array_slice($fibonacciNumbers, 6, 7);

print_r($finalFibonacciNumbers);

?>
