<?php

include('config.php');

// B. Change project status
function changeProjectStatus($projectId, $newStatus) {
    global $db;

    if ($newStatus === 'completed') {
        return "Cannot change completed status.";
    }

    $updateStatus = $db->prepare("UPDATE projects SET status = ? WHERE id = ?");
    $updateStatus->execute([$newStatus, $projectId]);

    return "Project status updated.";
}

// Example usage
echo changeProjectStatus(1, "in-progress");
