<?php
include ('config.php');

// C. List projects assigned to an engineer
function listProjectsForEngineer($engineerId) {
    global $db;

    $query = $db->prepare("SELECT * FROM projects WHERE developer_id = ?");
    $query->execute([$engineerId]);
    $projects = $query->fetchAll(PDO::FETCH_ASSOC);

    return $projects;
}

// Example Usage
print_r(listProjectsForEngineer(1));


?>