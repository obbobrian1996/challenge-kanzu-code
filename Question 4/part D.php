<?php

include('config.php');

// D. Change engineer assigned to the project
function changeEngineerForProject($projectId, $newEngineerId) {
    global $db;

    $updateEngineer = $db->prepare("UPDATE projects SET developer_id = ? WHERE id = ?");
    $updateEngineer->execute([$newEngineerId, $projectId]);

    return "Engineer updated for project.";
}


// Example Usage
echo changeEngineerForProject(1, 3);

?>