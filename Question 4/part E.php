<?php

include 'config.php';

// E. Mark project as completed (only PM)
function markProjectCompleted($projectId)
{
    global $db;

    // Check if the logged-in user is the PM
    $loggedInUserId = 1; // Example PM user ID

    $project = $db->prepare('SELECT * FROM projects WHERE id = ?');
    $project->execute([$projectId]);
    $projectInfo = $project->fetch(PDO::FETCH_ASSOC);

    if ($projectInfo['manager_id'] !== $loggedInUserId) {
        return 'Only the Project Manager can mark a project as completed.';
    }

    $updateStatus = $db->prepare('UPDATE projects SET status = ? WHERE id = ?');
    $updateStatus->execute(['completed', $projectId]);

    return 'Project marked as completed.';
}


//Example Usage

echo markProjectCompleted(1);
