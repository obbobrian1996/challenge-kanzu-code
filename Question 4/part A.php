<?php

include('config.php');

// A. Create a project and add a milestone
function createProject($title, $developerId, $managerId, $milestoneDescription) {
    global $db;

    $insertProject = $db->prepare("INSERT INTO projects (title, developer_id, manager_id, status) VALUES (?, ?, ?, ?)");
    $insertProject->execute([$title, $developerId, $managerId, 'awaiting-start']);
    $projectId = $db->lastInsertId();

    $insertMilestone = $db->prepare("INSERT INTO milestones (project_id, description, completed) VALUES (?, ?, ?)");
    $insertMilestone->execute([$projectId, $milestoneDescription, 0]);

    return "Project created with ID: " . $projectId;
}



// Example usage
echo createProject("New Project", 1, 2, "Initial Milestone");
echo createProject("Test Project", 2, 3, "Test Milestone");